//Obtenemos el paquete dotenv
require('dotenv').config();
const Server = require('./models/server')

// const express = require('express');

// const app = express();

// app.get('/', (req, res) => {
//  res.send('Hello World')
// })

// //Con process.env.PORT obtenemos el valor de la variable PORT que esta en el archivo .env
// //en ese archivo se guardan las variables sencibles
// app.listen(process.env.PORT, () => {
//   console.log('Servidor corriendo en puerto', process.env.PORT);
// });


const server = new Server();

server.listen();