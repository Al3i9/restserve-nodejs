const express = require('express');

class Server {

  constructor(){
    this.app = express();
    this.port = process.env.PORT
    this.usariosPath = '/api/usuarios';

    
    //Middlewares
    this.middlewares();

    this.routes()
  }

  middlewares(){

    //lectura y parseo del body. Va a hacerle una lectura a lo que pusimos en el body. 
    //Cualquier informacin que venga se serializara en formato JSON
    this.app.use(express.json());
    //Se publica la carpeta Public
    this.app.use(express.static('public'))
  }
  
  //responder con JSON
  routes() { //este es un middleware, porque tiene use
    this.app.use(this.usariosPath, require('../routes/user')); //aqui se quiere usar el archivo user, y que vaya con esa direccion
  }

  listen(){

    //Con process.env.PORT obtenemos el valor de la variable PORT que esta en el archivo .env
    this.app.listen(this.port, () => {
      console.log('Servidor corriendo en puerto ', this.port);
    })
  }
}

module.exports = Server;
