const { request } = require("express");

const { response } = 'express';

const usuarioGet = (req = request, res = response) => {

  //const query = req.query;
  const { nombre, key = 'no key' } = req.query;
  res.json({
    msg: 'get API - controlador',
    nombre,
    key
  });
};

const usuarioPut = (req, res = response) =>{

  // const id = req.params.id; //asi se obtiene el id del /routes/user. Que es el 10 que pusimos en el POstman, el  cual se puede cambiar
  //segunda manera de obtener el id, REestructuracion
  const { id } = req.params;

  res.json({
    msg: 'put API - controlador',
    id
  });
}

const usuarioPost = (req, res = response) =>{

  //req.body trae la informacion del body
  const {nombre, edad} = req.body; //aqui se creo una constante body, y en esta se almacena la respuesta (req.body) //luego se le cambio a una reestructuracion, para agarrar los elementos
  res.status(201).json({
    msg: 'post API - controlador',
    nombre,
    edad
  });
}

const usuarioDelete = (req, res = response) =>{
  res.json({
    msg: 'delete API - controlador'
  });
}


module.exports = {
  usuarioGet,
  usuarioPut,
  usuarioPost,
  usuarioDelete
}